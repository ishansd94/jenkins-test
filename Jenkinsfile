pipeline{

  agent any

  parameters {
    string(name: 'VERSION', defaultValue: '1.0.0', description: '')
  }

  stages{

    stage("Print Versions") {
      steps{
        withKubeCredentials([
                [credentialsId: 'kind-cluster-token', serverUrl: 'https://kind-control-plane:6443']
        ]) {
          sh '''
            docker -v
            kubectl version
            helm version
          '''
        }
      }
    }

    stage("Build Image") {
      steps{
        script {
          sh '''
            docker ps
            docker build -t docker-registry:5000/sample-app:${VERSION} .
            docker images
            docker push docker-registry:5000/sample-app:${VERSION}
          '''
        }
      }
    }

    stage("Test") {
      steps{
        echo "testing..."
      }
    }

    stage("Package") {
      steps{
        script {
          sh '''
            helm package ./deploy/Chart --app-version $VERSION --version $VERSION
            helm repo add chartmuseum http://helm-registry:8080
            curl --data-binary "@jenkins-test-${VERSION}.tgz" http://helm-registry:8080/api/charts
            helm repo update
            helm search repo -l
          '''
        }
      }
    }

    stage('Deploy') {
      steps{
        withKubeCredentials([
                [credentialsId: 'kind-cluster-token', serverUrl: 'https://kind-control-plane:6443']
        ]) {
          sh '''
            kubectl get pods -A
            helm upgrade -i jenkins-test chartmuseum/jenkins-test --version ${VERSION}
          '''
        }
      }
    }

  }

}
